import { Component, OnInit } from "@angular/core";
import { TodoService } from "src/app/todo.service";
import { ActivatedRoute } from "@angular/router";

export interface Todo {
  userId?: number;
  id?: number;
  title?: string;
  completed?: boolean;
}

@Component({
  selector: "app-todo-detail",
  templateUrl: "./todo-detail.page.html",
  styleUrls: ["./todo-detail.page.scss"]
})
export class TodoDetailPage implements OnInit {
  todo: Todo;

  constructor(
    private todoSvc: TodoService,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    console.log("TodoListDetail init");
    let id = this.activatedRoute.snapshot.paramMap.get("id");

    this.todoSvc.getTodoDetail(id).subscribe(data => {
      this.todo = data;
    });
  }
}
