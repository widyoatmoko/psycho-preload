import { TodoListPage } from './todo-list/todo-list.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'todos', pathMatch: 'full'},
  { path: 'todos', loadChildren: './todo-list/todo-list.module#TodoListPageModule' },
  // Uncomment this to unset preload for detail page
  // { path: 'todos/:id', loadChildren: './todo-detail/todo-detail.module#TodoDetailPageModule' },
  // Comment this to unset preload for detail page
  { path: 'todos/:id', loadChildren: './todo-detail/todo-detail.module#TodoDetailPageModule', data: {preload:true} },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
