import { Component, OnInit } from "@angular/core";
import { TodoService } from "./../../todo.service";

export interface Todo {
  userId?: number;
  id?: number;
  title?: string;
  completed?: boolean;
}

@Component({
  selector: "app-todo-list",
  templateUrl: "./todo-list.page.html",
  styleUrls: ["./todo-list.page.scss"]
})
export class TodoListPage implements OnInit {
  todos: any;

  constructor(private todoSvc: TodoService) {}

  ngOnInit() {
    console.log("TodoListPage init");
    this.todoSvc.getTodoList().subscribe(data => {
      this.todos = data;
    });
  }
}
