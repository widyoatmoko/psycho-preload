import { Injectable } from '@angular/core';
import { HttpClient, HttpInterceptor, HttpHandler, HttpResponse, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TodoService {
  BASE_URL = "https://jsonplaceholder.typicode.com";
  // BASE_URL = 'http://ulp.patikab.go.id/api_devel/api/evaluasi/list_paket_penawaran/'

  constructor(private http: HttpClient) { }

  getTodoList() {
    let httpUrl = this.BASE_URL + "/todos";
    // let httpUrl = this.BASE_URL;
    return this.http.get(httpUrl);
  }

  getTodoDetail(id) {
    let httpUrl = this.BASE_URL + "/todos/" + id;
    return this.http.get(httpUrl);
  }
}


// import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';

@Injectable()
export class AddHttpHeaderInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Get the auth header from the service.
    const authHeader = 'Bearer token_value';
    const clonedReq = req.clone({headers: req.headers.set('Authorization', authHeader)});
    return next.handle(clonedReq).pipe(
    map((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        const corsOrigin = 'hai-hai';
        const corsMethod = 'POST, GET, OPTIONS, PUT';
        // const clonedEvent = event.clone({headers: event.headers.set('access-control-allow-origin', corsOrigin)})
        event.headers.set('Access-Control-Allow-Origin', corsOrigin)
        event.headers.set('Access-Control-Allow-Methods', corsMethod)
        event.headers.set('Access-Control-Allow-Headers', 'accept, authorization, Content-Type')
        event = event.clone({headers: event.headers.set('tes', 'tis')});
        // event = clonedEvent.clone();
      }
        return event;
    })
    )
  }
}